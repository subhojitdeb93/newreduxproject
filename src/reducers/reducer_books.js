export default function() {
	return [
		{ title: 'Javascript: The good parts', pages: 120 },
		{ title: 'Harry Potter', pages: 100  },
		{ title: 'Sherlock Holmes', pages: 150 },
		{ title: 'The Immortals of Meluha', pages: 220  }
	];
}